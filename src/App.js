import "./App.css";

import Header from "./components/Header";
import Footer from "./components/Footer";
import Content from "./components/Content/Content";



function App() {
  return (
    <div id="the-master">
      {/* Navbar */}
      <Header/>
      
      <div id="home" className="container p-4 mt-4">
        <Content/>
      </div>

      {/* Footer */}
      <Footer/>
    </div>
  );
}

export default App;
