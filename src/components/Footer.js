import {
    FaArrowUp,
    FaFacebook,
    FaInstagram,
    FaSnapchat,
    FaPinterest,
    FaTwitter,
    FaLinkedin,
  } from "react-icons/fa";

export default function Footer () {
        return (
            <div className="container-fluid p-5 mt-5 bg-orange">
                <div className="row text-center">
                    <div className="col-sm-12">
                        <h4 className="m-2">Footer</h4>
                        <a href="#home" className="btn btn-dark m-3"><FaArrowUp />To the top</a>
                        <div className="m-2">
                            <FaFacebook /> <span> </span>
                            <FaInstagram /> <span> </span>
                            <FaSnapchat /> <span> </span>
                            <FaPinterest /> <span> </span>
                            <FaTwitter /> <span> </span>
                            <FaLinkedin />
                        </div>
                        <b>Powered by Devcamp</b>
                    </div>
                </div>
            </div>
        )
    }
