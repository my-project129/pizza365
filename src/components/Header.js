import { Component } from "react";

import { Container, Grid } from '@mui/material/';

class Header extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Grid>
                        <div className="col-12">
                            <nav className="navbar navbar-light bg-orange navbar-expand-lg fixed-top">
                                <ul className="navbar-nav nav-fill w-100">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="#home">Trang chủ <span className="sr-only">(current)</span></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#combo">Combo</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#pizza-type">Loại pizza</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#send-order">Gửi đơn hàng</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default Header;