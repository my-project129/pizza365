
import Lading from "./Content components/Lading";
import Info from "./Content components/Info";
import PizzaSize from "./Content components/PizzaSize";
import PizzaType from "./Content components/PizzaType";
import DrinkSelect from "./Content components/DrinkSelect";
import FormSubmit from "./Content components/FormSubmit";
import ButtonSubmit from "./Content components/ButtonSubmit";

import { useState } from "react";
import { Snackbar, Alert } from "@mui/material"; 

export default function Content() {

    // Tạo hook state lưu dữ liệu component pizzaSiza
    const [pizzaSize, setPizzaSize] = useState({
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        soLuongNuoc: "",
        thanhTien: 0,
    })

    // Tạo HookState chứa dữ liệu Component  Pizza Type
    const [pizzaType, setPizzaType] = useState("")

    // Tạo HookState để chứa dữ liệu đồ uống
    const [valueDrink, setValueDrink] = useState('');

    // Tạo HookState để lưu dữ liệu người dùng nhập vào trên form
    const [userDetail, setUserDetail] = useState({
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
        idVourcher: "",
    })


    // Phần alrert
    const [open, setOpen] = useState(false);
    const [alertStatus, setAlertStatus] = useState("error");
    const [alertMessage, setAlertMessage] = useState("")
    const openSnackBar = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    return (
        <>
            <Lading />
            <Info />
            <PizzaSize setPizzaSize={setPizzaSize} />
            <PizzaType  setPizzaType={setPizzaType} />
            <DrinkSelect  setValueDrink={setValueDrink}/>
            <FormSubmit userDetail={userDetail} setUserDetail={setUserDetail}/>
            <ButtonSubmit pizzaSize={pizzaSize} pizzaType={pizzaType} valueDrink={valueDrink} userDetail={userDetail}
            setAlertStatus={setAlertStatus} setAlertMessage={setAlertMessage} openSnackBar={openSnackBar}/>

            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}
                    anchorOrigin={{ vertical: "top", horizontal: "center" }} >
                <Alert onClose={handleClose} severity={alertStatus} sx={{ width: '100%' }}>
                    {alertMessage}
                </Alert>
            </Snackbar>
        </>
    )

}
