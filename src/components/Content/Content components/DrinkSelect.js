import { useState, useEffect } from "react";

export default function DrinkSelect({ setValueDrink }) {
  // Tạo HookState để chứa mảng drink sau khi gọi từ sever
  const [dataDrink, setDataDrink] = useState([]);

  const getDrinkList = async () => {
    const response = await fetch(
      "http://42.115.221.44:8080/devcamp-pizza365/drinks"
    );
    const responseData = await response.json();
    return responseData;
  };

  useEffect(() => {
    getDrinkList()
      .then((data) => {
        setDataDrink(data);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }, []);

  const onSelectDrinkChange = (event) => {
    let bDrinkSelected = event.target.value;
    console.log(bDrinkSelected);
    setValueDrink(bDrinkSelected);
  };

  return (
    <>
      <div className="text-center mt-4 p-4">
        <h2>
          <b
            className="p-2 txt-orange"
            style={{ borderBottom: "1px solid orange" }}
          >
            Chọn đồ uống
          </b>
        </h2>
      </div>
      <div className="col-12">
        <select
          id="select-drink"
          className="form-control"
          onChange={onSelectDrinkChange}
        >
          <option value="">Tất cả loại nước uống</option>
          {/* {dataDrink.map((element, index) => {
                        return (
                            <option key={index} value={element.maNuocUong}>{element.tenNuocUong}</option>
                        )
                    })} */}
          <option value="TRATAC">Trà tắc</option>
          <option value="COCA">Cocacola</option>
          <option value="PEPSI">Pepsi</option>
          <option value="LAVIE">Lavie</option>
          <option value="TRASUA">Trà sữa trân châu</option>
          <option value="FANTA">Fanta</option>
        </select>
      </div>
    </>
  );
}
