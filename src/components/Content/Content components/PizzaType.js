import $ from "jquery";

import SeafoodImg from "../../../assets/images/seafood.jpg";
import HawaiianImg from "../../../assets/images/hawaiian.jpg";
import BaconImg from "../../../assets/images/bacon.jpg";

export default function PizzaType({setPizzaType}) {


    const onClickBtnHaiSanHandler = () => {
        console.log("Pizza Hai San is selected");
        setPizzaTypeButtonColor("HAI SAN") //Đổi màu
        setPizzaType("HAI SAN")
    }

    const onClickBtnHawaiiHandler = () => {
        console.log("Pizza HAWAII is selected");
        setPizzaTypeButtonColor("HAWAII") //Đổi màu
        setPizzaType("HAWAII");
    }

    const onClickBtnBaconHandler = () => {
        console.log("Pizza HUN KHOI is selected");
        setPizzaTypeButtonColor("HUN KHOI") //Đổi màu
        setPizzaType("HUN KHOI");
    }

    // Khai báo hàm đổi màu nút Loại pizza
    const setPizzaTypeButtonColor = (paramLoaiPizza) => {
        // Truy xuất đến 3 phần tử nút bấm
        var vBtnHaiSanElement = $("#btn-haisan");
        var vBtnHawaiiElement = $("#btn-hawaii");
        var vBtnBaconElement = $("#btn-bacon");

        // Đổi màu nút bằng cách đổi className
        if (paramLoaiPizza === "HAI SAN") {
            $(vBtnHaiSanElement).prop("class", "btn btn-block btn-success");
            $(vBtnHawaiiElement).prop("class", "btn btn-block btn-warning");
            $(vBtnBaconElement).prop("class", "btn btn-block btn-warning");
        }
        else if (paramLoaiPizza === "HAWAII") {
            $(vBtnHaiSanElement).prop("class", "btn btn-block btn-warning");
            $(vBtnHawaiiElement).prop("class", "btn btn-block btn-success");
            $(vBtnBaconElement).prop("class", "btn btn-block btn-warning");
        }
        else if (paramLoaiPizza === "HUN KHOI") {
            $(vBtnHaiSanElement).prop("class", "btn btn-block btn-warning");
            $(vBtnHawaiiElement).prop("class", "btn btn-block btn-warning");
            $(vBtnBaconElement).prop("class", "btn btn-block btn-success");
        }
    }

    return (
        <div id="pizza-type" className="p-4">
            {/* Title Chọn loại Pizza */}
            <div className="text-center mt-4 p-4">
                <h2><b className="p-2 txt-orange" style={{ borderBottom: "1px solid orange" }}>Chọn loại pizza</b></h2>
            </div>
            {/* Content Chọn loại Pizza */}
            <div className="row">
                <div className="col-sm-4">
                    <div className="card w-100" style={{ width: "18rem" }}>
                        <img src={SeafoodImg} alt="seafood" className="card-img-top" />
                        <div className="card-body ">
                            <h4 className="text-uppercase">ocean mania</h4>
                            <p className="text-uppercase">Pizza hải sản sốt mayonnase</p>
                            <p>Sốt cà chua, phô mai Mozzarella, tôm, mực, thanh cua, hành tây.
                            </p>
                            <p><button id="btn-haisan" className="btn btn-warning btn-block" onClick={onClickBtnHaiSanHandler}>Chọn</button></p>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card w-100 h-100" style={{ width: "18rem" }}>
                        <img src={HawaiianImg} alt="hawaiian" className="card-img-top" />
                        <div className="card-body">
                            <h4 className="text-uppercase">hawaiian</h4>
                            <p className="text-uppercase">Pizza dăm bông dứa kiểu hawaii</p>
                            <p>Sốt cà chua, phô mai Mozzarella, thịt dăm bông, thơm.
                            </p>
                            <p><button id="btn-hawaii" className="btn btn-warning btn-block" onClick={onClickBtnHawaiiHandler}>Chọn</button></p>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card w-100 h-100" style={{ width: "18rem" }}>
                        <img src={BaconImg} alt="bacon" className="card-img-top" />
                        <div className="card-body">
                            <h4 className="text-uppercase">chessy chicken bacon</h4>
                            <p className="text-uppercase">Pizza gà phô mai thịt heo xông khói</p>
                            <p>Sốt phô mai, thịt gà, thịt heo muối, phô mai Mozzarella, cà chua.
                            </p>
                            <p><button id="btn-bacon" className="btn btn-warning btn-block" onClick={onClickBtnBaconHandler}>Chọn</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}