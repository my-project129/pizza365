export default function Info() {
  return (
    <>
      {/* Title Tại sao chọn pizza 365  */}
      <div className="text-center mt-4 p-4">
        <h2>
          <b
            className="p-2 txt-orange"
            style={{ borderBottom: "1px solid orange" }}
          >
            Tại sao lại Pizza 365
          </b>
        </h2>
      </div>
      {/* Content Tại sao chọn pizza 365 */}
      <div className="row">
        <div
          className="col p-4"
          style={{
            backgroundColor: "lightgoldenrodyellow",
            border: "1px solid orange",
          }}
        >
          <h3 className="p-2">Đa dạng</h3>
          <p className="p-2">
            Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện
            nay
          </p>
        </div>
        <div
          className="col p-4"
          style={{ backgroundColor: "yellow", border: "1px solid orange" }}
        >
          <h3 className="p-2">Chất lượng</h3>
          <p className="p-2">
            Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ
            sinh an toàn thực phẩm
          </p>
        </div>
        <div
          className="col p-4"
          style={{ backgroundColor: "lightsalmon", border: "1px solid orange" }}
        >
          <h3 className="p-2">Hương vị</h3>
          <p className="p-2">
            Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ
            Pizza 365
          </p>
        </div>
        <div
          className="col p-4"
          style={{ backgroundColor: "orange", border: "1px solid yellow" }}
        >
          <h3 className="p-2">Dịch vụ</h3>
          <p className="p-2">
            Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh,
            chất lượng, tân tiến
          </p>
        </div>
      </div>
    </>
  );
}
