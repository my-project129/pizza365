import ConfirmModal from "./confirmModal/ConfirmModal";
import { useState } from "react";

export default function ButtonSubmit ({pizzaSize, pizzaType, valueDrink, userDetail, setAlertStatus, setAlertMessage, openSnackBar}) {

    let vObjectRequest = {
        kichCo: pizzaSize.kichCo,
        duongKinh: pizzaSize.duongKinh,
        suon: pizzaSize.suon,
        salad: pizzaSize.salad,
        loaiPizza: pizzaType,
        idVourcher: userDetail.idVourcher,
        idLoaiNuocUong: valueDrink,
        soLuongNuoc: pizzaSize.soLuongNuoc,
        hoTen: userDetail.hoTen,
        thanhTien: pizzaSize.thanhTien,
        email: userDetail.email,
        soDienThoai: userDetail.soDienThoai,
        diaChi: userDetail.diaChi,
        loiNhan: userDetail.loiNhan
    }

    const validateData = (paramOrderData) => {

        if (paramOrderData.kichCo === "") {
            setAlertStatus("error");
            setAlertMessage("bạn chưa chọn kích cỡ pizza");
            openSnackBar();
            return false;
        }
        if (paramOrderData.loaiPizza === "") {
            setAlertStatus("error");
            setAlertMessage("bạn chưa chọn loại pizza");
            openSnackBar();
            return false
        }
        if (paramOrderData.hoTen === "") {
            setAlertStatus("error");
            setAlertMessage("bạn chưa nhập vào họ tên");
            openSnackBar();
            return false
        }
        if (paramOrderData.email === "") {
            setAlertStatus("error");
            setAlertMessage("bạn chưa nhập vào email");
            openSnackBar();
            return false
        }
        if (paramOrderData.soDienThoai === "") {
            setAlertStatus("error");
            setAlertMessage("bạn chưa nhập vào số điện thoại");
            openSnackBar();
            return false
        }
        if (paramOrderData.diaChi === "") {
            setAlertStatus("error");
            setAlertMessage("bạn chưa nhập vào địa chỉ");
            openSnackBar();
            return false
        }
        return true;
    }
    
    const onClickBtnSubmitHandle = () => {
        console.log(vObjectRequest);
        // B1: Thu thập dữ liệu: là gOrderData
        // B2: Kiểm tra dữ liệu
        var vIsDataValidate = validateData(vObjectRequest);
        if (vIsDataValidate) {
            // Mở modal xác nhận gửi đơn hàng
            handleConfirmModalOpen();           
        }
    }

    const [openConfirmModal, setOpenConfirmModal] = useState(false);
    const handleConfirmModalOpen = () => setOpenConfirmModal(true);
    const handleConfirmModalClose = () => setOpenConfirmModal(false);

    return (
        <>
        <button id="btn-send-order" type="button" className="btn btn-warning btn-block" onClick={onClickBtnSubmitHandle} >Gửi</button>
        <ConfirmModal openConfirmModal={openConfirmModal} handleConfirmModalClose={handleConfirmModalClose} vObjectRequest={vObjectRequest}/>
        </>
    )
}