
export default function FormSubmit({userDetail, setUserDetail}) {

    const onChangeInpHoTenHandle = (event) => {
        setUserDetail(userDetail=>({
            ...userDetail,
            ["hoTen"]: event.target.value
         }))
    }
    const onChangeInpEmailHandle = (event) => {
        setUserDetail(userDetail=>({
            ...userDetail,
            ["email"]: event.target.value
         }))
    }
    const onChangeInpSoDienThoaiHandle = (event) => {
        setUserDetail(userDetail=>({
            ...userDetail,
            ["soDienThoai"]: event.target.value
         }))
    }
    const onChangeInpDiaChiHandle = (event) => {
        setUserDetail(userDetail=>({
            ...userDetail,
            ["diaChi"]: event.target.value
         }))
    }
    const onChangeInpLoiNhanHandle = (event) => {
        setUserDetail(userDetail=>({
            ...userDetail,
            ["loiNhan"]: event.target.value
         }))
    }
    const onChangeInpMaGiamGiaHandle = (event) => {
        setUserDetail(userDetail=>({
            ...userDetail,
            ["idVourcher"]: event.target.value
         }))
    }

    return (
        <div id="send-order" className="p-4">
            {/* Title Contact Us */}
            <div className="text-center mt-4 p-4">
                <h2><b className="p-2 txt-orange" style={{ borderBottom: "1px solid orange" }}>Gửi đơn hàng</b></h2>
            </div>
            {/* Content Contact Us */}
            <div className="row">
                <div className="col-sm-12">
                    <div className="form-group">
                        <label for="fullname">Họ và tên <span className="text-danger">(*)</span></label>
                        <input type="text" className="form-control" id="inp-fullname" placeholder="Họ và tên"
                            value={userDetail.hoTen} onChange={onChangeInpHoTenHandle} />
                    </div>
                    <div className="form-group">
                        <label for="email">Email <span className="text-danger">(*)</span></label>
                        <input type="text" className="form-control" id="inp-email" placeholder="Email"
                            value={userDetail.email} onChange={onChangeInpEmailHandle} />
                    </div>
                    <div className="form-group">
                        <label for="dien-thoai">Điện thoại <span className="text-danger">(*)</span></label>
                        <input type="number" className="form-control" id="inp-dien-thoai" placeholder="Điện thoại"
                            value={userDetail.soDienThoai} onChange={onChangeInpSoDienThoaiHandle} />
                    </div>
                    <div className="form-group">
                        <label for="dia-chi">Địa chỉ <span className="text-danger">(*)</span></label>
                        <input type="text" className="form-control" id="inp-dia-chi" placeholder="Địa chỉ"
                            value={userDetail.diaChi} onChange={onChangeInpDiaChiHandle} />
                    </div>
                    <div className="form-group">
                        <label for="message">Lời nhắn</label>
                        <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn"
                            value={userDetail.loiNhan} onChange={onChangeInpLoiNhanHandle} />
                    </div>
                    <div className="form-group">
                        <label for="voucher">Mã giảm giá</label>
                        <input type="text" className="form-control" id="inp-voucher" placeholder="Mã giảm giá"
                            value={userDetail.idVourcher} onChange={onChangeInpMaGiamGiaHandle} />
                    </div>
                </div>
            </div>
        </div>
    )
}