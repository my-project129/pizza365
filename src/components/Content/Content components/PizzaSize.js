import $ from 'jquery';

export default function PizzaSize({setPizzaSize}) {

            
    const setMenuButtonColor = (paramKichCo) => {
        // Truy xuất đến 3 phần tử nút bấm
        var vBtnSmallElement = $("#btn-small");
        var vBtnMediumElement = $("#btn-medium");
        var vBtnLargeElement = $("#btn-large");
      
        // Đổi màu nút bằng cách đổi className
        if (paramKichCo === "S") {
          $(vBtnSmallElement).prop("class","btn btn-block btn-success");
          $(vBtnMediumElement).prop("class","btn btn-block btn-warning");
          $(vBtnLargeElement).prop("class","btn btn-block btn-warning");
        }
        else if (paramKichCo === "M") {
          $(vBtnSmallElement).prop("class","btn btn-block btn-warning");
          $(vBtnMediumElement).prop("class","btn btn-block btn-success");
          $(vBtnLargeElement).prop("class","btn btn-block btn-warning");
        }
        else if (paramKichCo === "L") {
          $(vBtnSmallElement).prop("class","btn btn-block btn-warning");
          $(vBtnMediumElement).prop("class","btn btn-block btn-warning");
          $(vBtnLargeElement).prop("class","btn btn-block btn-success");
        }
      }

    const onClickBtnSmallHandler = () => {
        console.log("Size S is selected");

        setPizzaSize({
            kichCo: 'S',
            duongKinh: '20',
            suon: '2',
            salad: '200',
            soLuongNuoc: '2',
            thanhTien: 150000
        });

        setMenuButtonColor("S"); // Đổi màu nút
    }

    const onClickBtnMediumHandler = () => {
        console.log("Size M is selected");

        setPizzaSize({
            kichCo: 'M',
            duongKinh: '25',
            suon: '4',
            salad: '300',
            soLuongNuoc: '3',
            thanhTien: 200000
        });

        setMenuButtonColor("M"); // Đổi màu nút
    }

    const onClickBtnLargeHandler = () => {
        console.log("Size L is selected");

        setPizzaSize({
            kichCo: 'L',
            duongKinh: '30',
            suon: '8',
            salad: '500',
            soLuongNuoc: '4',
            thanhTien: 250000
        });

        setMenuButtonColor("L"); // Đổi màu nút
    }

    return (
        <div id="combo" className="p-4">
            {/* Title Menu combo pizza 365 */}
            <div className="text-center mt-4 p-4">
                <h2><b className="p-2 txt-orange" style={{ borderBottom: "1px solid orange" }}>Chọn size pizza</b></h2>
                <p className="mt-3"><span className="text-warning">Chọn combo pizza phù hợp với nhu cầu của bạn.</span></p>
            </div>
            {/* Content Menu Combo Pizza 365 */}
            <div className="col-sm-12">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="card">
                            <div className="card-header text-white text-center" style={{ backgroundColor: "darkorange" }}>
                                <h3>S (small)</h3>
                            </div>
                            <div className="card-body text-center">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>20 cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>2</b></li>
                                    <li className="list-group-item">Salad: <b>200 g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>2</b></li>
                                    <li className="list-group-item"><h1><b>150.000</b></h1>VNĐ</li>
                                </ul>
                            </div>
                            <div className="card-footer text-center">
                                <button id="btn-small" className="btn btn-warning btn-block" onClick={onClickBtnSmallHandler}>Chọn</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="card">
                            <div className="card-header text-white text-center" style={{ backgroundColor: "orange" }}>
                                <h3>M (medium)</h3>
                            </div>
                            <div className="card-body text-center">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>25 cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>4</b></li>
                                    <li className="list-group-item">Salad: <b>300 g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>3</b></li>
                                    <li className="list-group-item"><h1><b>200.000</b></h1>VNĐ</li>
                                </ul>
                            </div>
                            <div className="card-footer text-center">
                                <button id="btn-medium" className="btn btn-warning btn-block" onClick={onClickBtnMediumHandler}>Chọn</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="card">
                            <div className="card-header text-white text-center" style={{ backgroundColor: "darkorange" }}>
                                <h3>L (large)</h3>
                            </div>
                            <div className="card-body text-center">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>30 cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>8</b></li>
                                    <li className="list-group-item">Salad: <b>500 g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>4</b></li>
                                    <li className="list-group-item"><h1><b>250.000</b></h1>VNĐ</li>
                                </ul>
                            </div>
                            <div className="card-footer text-center">
                                <button id="btn-large" className="btn btn-warning btn-block" onClick={onClickBtnLargeHandler}>Chọn</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
