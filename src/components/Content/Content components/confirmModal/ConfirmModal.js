import { Modal, Box, Typography, Button, Grid } from "@mui/material"
import { useState } from "react"
import Modal2 from "./Modal2";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function ConfirmModal({openConfirmModal, handleConfirmModalClose, vObjectRequest}) {

    // Tạo promise xử lí bất đồng bộ
    const fetchApi = async(url, body) => {
        const response = await fetch(url, body);
        const responseData = await response.json();
        return responseData;
    };
    let body = {
        method: "POST",
        body: JSON.stringify(vObjectRequest),
        headers: {
            "Content-Type": "application/json;charset=UTF-8"
        },
    }

    // Tạo hook state chứa orderId
    const [responseOrderId, setResponseOrderId] = useState('')

    const onClickConfirmHandle = () => {
        // B3: Gọi api thêm đơn hàng
        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders', body)
            .then((data) => {
                console.log(data);
                // B4: Xử lí hiển thị
                // Tắt modal cũ
                handleConfirmModalClose();
                // Mở modal mới
                setResponseOrderId(data.orderId)
                handleOpenModal2();

            })
            .catch((error) => {
                console.log(error.message)
            })
    }

    const [openModal2, setOpenModal2] = useState(false);
    const handleOpenModal2 = () => setOpenModal2(true);
    const handleCloseModal2 = () => setOpenModal2(false);

    return (
        <div>
            <Modal
                open={openConfirmModal}
                onClose={handleConfirmModalClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2" textAlign={"center"}>
                        Bạn có muốn đặt đơn hàng này không?
                    </Typography>
                    <Grid item xs={12} textAlign='center'>
                        <Button variant="contained" sx={{ marginInline: '10px' }} color="success" onClick={onClickConfirmHandle} >
                            Xác nhận
                        </Button>
                        <Button variant="contained" onClick={handleConfirmModalClose} sx={{ width: '107px' }} >
                            Hủy bỏ
                        </Button>
                    </Grid>
                </Box>
            </Modal>

            <Modal2 openModal2={openModal2} handleCloseModal2={handleCloseModal2} responseOrderId={responseOrderId}/>
        </div>
    );
}