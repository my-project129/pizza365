import { Component } from "react";
import Carousel from "react-bootstrap/Carousel";

import FirstSlide from "../../../assets/images/1.jpg";
import SecondSlide from "../../../assets/images/2.jpg";
import ThirdSlide from "../../../assets/images/3.jpg";
import FourthSlide from "../../../assets/images/4.jpg";

class Lading extends Component {
  render() {
    return (
      <>
        {/* Title  */}
        <h1 className="mt-4 p-4">
          <p className="text-warning">
            <b>PIZZA 365</b>
          </p>
          <p className="h5" style={{ color: "coral" }}>
            <i>Truly Italian !</i>
          </p>
        </h1>
        {/* Slide */}
        <Carousel>
          <Carousel.Item>
            <img className="d-block w-100" src={FirstSlide} alt="First slide" />
            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={SecondSlide}
              alt="Second slide"
            />

            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img className="d-block w-100" src={ThirdSlide} alt="Third slide" />

            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={FourthSlide}
              alt="Third slide"
            />

            <Carousel.Caption></Carousel.Caption>
          </Carousel.Item>
        </Carousel>

        {/* <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img className="d-block w-100" src={FirstSlide} alt="First slide"></img>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={SecondSlide} alt="Second slide"></img>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={ThirdSlide} alt="Third slide"></img>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={FourthSlide} alt="Fourth slide"></img>
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div> */}
      </>
    );
  }
}

export default Lading;
